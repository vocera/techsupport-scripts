# Technical Support Scripts

This repository contains scripts developed by Vocera Technical Support for the purposes of
improving customer support.  This may be scripts developed to meet a specific customer need,
or they may be designed to help Technical Support Engineers (TSEs) respond to, and diagnose, customer situations quicker

